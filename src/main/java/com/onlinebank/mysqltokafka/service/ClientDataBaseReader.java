package com.onlinebank.mysqltokafka.service;

import com.onlinebank.mysqltokafka.Client;
import com.onlinebank.mysqltokafka.kafka.ClientDataProducer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import java.util.List;
@Component
public class ClientDataBaseReader {
    private static final Logger log = LoggerFactory.getLogger(ClientDataBaseReader.class);
    private static final String SELECT_QUERY = "SELECT id, fullname,  login, password, email, phone FROM users";
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private ClientDataProducer clientDataProducer;
    @Scheduled(fixedRate = 30000) // задаем интервал отправки сообщений в миллисекундах
    public List<Client> readClientFromDataBase() {
        log.info("ReadClientFromDataBase in db execute ");
        List<Client> clients = jdbcTemplate.query(SELECT_QUERY, new ClientRowMapper());
        clientDataProducer.sendClientToTopic(clients);
        return clients;
    }
}
