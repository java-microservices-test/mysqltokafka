package com.onlinebank.mysqltokafka.service;

import com.onlinebank.mysqltokafka.Client;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientRowMapper implements RowMapper <Client> {
    @Override
    public Client mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Client client = new Client();
        client.setId(resultSet.getString("id"));
        client.setFullname(resultSet.getString("fullname"));
        client.setEmail(resultSet.getString("email"));
        client.setPhone(resultSet.getString("phone"));
        client.setLogin(resultSet.getString("login"));
        client.setPassword(resultSet.getString("password"));
        return client;

    }
}
