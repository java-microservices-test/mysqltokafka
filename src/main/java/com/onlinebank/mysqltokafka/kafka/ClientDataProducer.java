package com.onlinebank.mysqltokafka.kafka;

import com.onlinebank.mysqltokafka.Client;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClientDataProducer {
    private static final Logger log = LoggerFactory.getLogger(KafkaProducer.class);
    private static final String topicName = "wallet-users";
    @Autowired
    private KafkaTemplate<String, Client> kafkaTemplate;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate1;

    public void sendClientToTopic(List<Client> clients) {
        log.info("Sending {} clients to topic {}", clients.size(), topicName);
        for (Client client : clients) {
            kafkaTemplate.send(topicName, client);
        }
    }

    public void sendClientToTopicTest(String message) {
        log.info("Sending {} clients to topic {}", message, topicName);
        log.info("Sending client {} to topic {}", topicName);
        kafkaTemplate1.send("client", "provet");


    }
}
