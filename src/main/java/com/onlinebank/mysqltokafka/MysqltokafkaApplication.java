package com.onlinebank.mysqltokafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MysqltokafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MysqltokafkaApplication.class, args);
    }

}
