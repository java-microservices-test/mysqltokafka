package com.onlinebank.mysqltokafka;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Client {
    private String id;

    private String fullname;

    private String email;

    private String phone;

    private String login;

    private String password;
}
